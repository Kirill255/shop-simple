# shop-simple

## Run the app in the development mode

`npm start`

## Other

[Absolute Imports](https://create-react-app.dev/docs/importing-a-component#absolute-imports)

## Contributors

<!-- prettier-ignore -->
<table><tr><td align="center"><a href="https://gitlab.com/Kirill255"><img src="https://gitlab.com/uploads/-/system/user/avatar/2418447/avatar.png?width=400" width="100px;" alt="Kirill255"/><br /><sub><b>Kirill255</b></sub></a><br /></td><td align="center"><a href="https://gitlab.com/Simon0925"><img src="https://secure.gravatar.com/avatar/812566d347bbc597125399e330213e48?s=800&d=identicon" width="100px;" alt="Semen Yakovenko"/><br /><sub><b>Semen Yakovenko</b></sub></a><br /></td></tr></table>
