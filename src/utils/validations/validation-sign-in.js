export default function validateReg(values) {
  let errors = {};
  if (!values.email) {
    errors.email = "E-mail required";
  } else if (values.email.length < 2) {
    errors.email = "E-mail not found";
  }

  if (!values.password) {
    errors.password = "Password required";
  } else if (values.password > 1) {
    errors.password = "Enter corect your password";
  }
  return errors;
}
