export default function validateReg(values) {
  let errors = {};
  if (!values.name) {
    errors.name = "Name required";
  } else if (values.name.length < 2) {
    errors.name = "Title must be at least 2 characters";
  }

  if (!values.email) {
    errors.email = "E-mail required";
  } else if (values.email.indexOf("@") === -1) {
    errors.email = "E-mail must have `@`";
  }

  if (!values.phone) {
    errors.phone = "Phone required";
  } else if (values.phone.length < 8) {
    errors.phone = "Phone must have 8 numbers ";
  }
  if (!values.password) {
    errors.password = "Phone required";
  } else if (values.password.length < 8) {
    errors.password = "Password must have 8 numbers ";
  }
  if (!values.password2) {
    errors.password2 = "Phone required";
  } else if (values.password !== values.password2) {
    errors.password2 = "Password2 must be equal password";
  }

  return errors;
}
