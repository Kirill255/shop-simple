export default function validateBook(values) {
  let errors = {};

  if (!values.title) {
    errors.title = "Title required";
  } else if (values.title.length < 2) {
    errors.title = "Title must be at least 2 characters";
  }

  if (!values.author) {
    errors.author = "Author required";
  } else if (values.author.length < 2) {
    errors.author = "Author must be at least 2 characters";
  }

  // const isNumber = n => typeof n == 'number' && !isNaN(n);
  if (!values.price) {
    errors.price = "Price required";
  } else if (+values.price <= 0) {
    errors.price = "Price can't be 0 or less";
  }

  if (!values.coverImage) {
    errors.coverImage = "Cover image required";
  } else if (!/^(http|https):\/\/[^ "]+$/.test(values.coverImage)) {
    errors.coverImage = "Cover image url must be valid";
  }

  return errors;
}
