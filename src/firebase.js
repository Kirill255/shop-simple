import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyDsdOs2UlwcdkFug8dMxXUuC6kseFTlz_0",
  authDomain: "books-3f8e0.firebaseapp.com",
  databaseURL: "https://books-3f8e0.firebaseio.com",
  projectId: "books-3f8e0",
  storageBucket: "books-3f8e0.appspot.com",
  messagingSenderId: "150704908833",
  appId: "1:150704908833:web:4d459ff479a051f6"
});

export const db = firebaseApp.firestore();

export default firebaseApp;
