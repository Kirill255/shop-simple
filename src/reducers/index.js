import { combineReducers } from "redux";
import bookList from "./book-list";
import shoppingCart from "./shopping-cart";
import users from "./users";
import rightMenuRed from "./rightMenuRed";

export default combineReducers({
  bookList,
  shoppingCart,
  users,
  rightMenuRed
});
