import { HOVER_MENU, HOVER_MENU_OUT } from "../constants";

const INITIAL_STATE = {
  isHovered: false
};
export default (state = INITIAL_STATE, { type }) => {
  switch (type) {
    case HOVER_MENU:
      return {
        ...state,
        isHovered: false
      };
    case HOVER_MENU_OUT:
      return {
        ...state,
        isHovered: true
      };
    default:
      return state;
  }
};
