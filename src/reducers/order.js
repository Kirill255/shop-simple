import { PASS_ALL_BOOKS_FROM_BASKET } from "../constants";

const INITIAL_STATE = {
  order: [],
  loading: false, // true ?
  error: null
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case PASS_ALL_BOOKS_FROM_BASKET:
      const newOrder = [...state.order, payload];
      return {
        order: newOrder,
        loading: false,
        error: null
      };
    default:
      return state;
  }
};
