import {
  ADD_BOOK_TO_CART,
  REMOVE_BOOK_FROM_CART,
  REMOVE_ALL_BOOK_FROM_CART,
  PASS_ALL_BOOKS_FROM_BASKET,
  CLEAN_ORDER
} from "../constants";

const INITIAL_STATE = {
  cartItems: [],
  totalOrder: 0
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case ADD_BOOK_TO_CART:
      console.log("asdads");
      // добавили только id
      return {
        ...state,
        cartItems: [...state.cartItems, payload]
      };

    case REMOVE_BOOK_FROM_CART:
      let updatedCartItems;
      const idx = state.cartItems.findIndex(id => id === payload);
      if (idx !== -1) {
        updatedCartItems = [
          ...state.cartItems.slice(0, idx),
          ...state.cartItems.slice(idx + 1)
        ];
      } else {
        updatedCartItems = [...state.cartItems];
      }

      return {
        ...state,
        cartItems: updatedCartItems
      };

    case REMOVE_ALL_BOOK_FROM_CART: {
      const updatedCartItems = state.cartItems.filter(id => id !== payload);
      return {
        ...state,
        cartItems: updatedCartItems
      };
    }
    case PASS_ALL_BOOKS_FROM_BASKET:
      return {
        ...state,
        cartItems: [...state.cartItems]
      };
    case CLEAN_ORDER:
      console.log("CLEAN_ORDER");

      return {
        ...state,
        cartItems: []
      };

    default:
      return state;
  }
};
