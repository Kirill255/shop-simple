import { ADD_NEW_USER } from "../constants";

const INITIAL_STATE = {
  users: [],
  loading: false,
  error: null
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case ADD_NEW_USER:
      console.log(1);
      const newUsers = [...state.users, payload];
      return {
        users: newUsers,
        loading: false,
        error: null
      };
    default:
      return state;
  }
};
