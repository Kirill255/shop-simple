import {
  FETCH_BOOKS_REQUEST,
  FETCH_BOOKS_SUCCESS,
  FETCH_BOOKS_FAIL,
  // ADD_NEW_BOOK_REQUEST,
  ADD_NEW_BOOK_SUCCESS,
  EDIT_BOOK_SUCCESS,
  DELETE_BOOK_SUCCESS
} from "../constants";

const INITIAL_STATE = {
  books: [],
  loading: false, // true ?
  error: null
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case FETCH_BOOKS_REQUEST:
      return {
        books: [],
        loading: true,
        error: null
      };

    case FETCH_BOOKS_SUCCESS:
      return {
        books: payload,
        loading: false,
        error: null
      };

    case FETCH_BOOKS_FAIL:
      return {
        books: [],
        loading: false,
        error: payload
      };

    case ADD_NEW_BOOK_SUCCESS:
      const newBooks = [...state.books, payload];
      return {
        books: newBooks,
        loading: false,
        error: null
      };

    case EDIT_BOOK_SUCCESS: {
      // const index = state.books.findIndex((book) => book.id === payload.id);
      // const updatedBooks = [...state.books.splice(index, 1, payload.book)];
      const index = state.books.findIndex(book => book.id === payload.id);
      const updatedBooks = [
        ...state.books.slice(0, index),
        payload.book,
        ...state.books.slice(index + 1)
      ];
      return {
        books: updatedBooks,
        loading: false,
        error: null
      };
    }

    case DELETE_BOOK_SUCCESS:
      const updatedBooks = state.books.filter(book => book.id !== payload);
      return {
        books: updatedBooks,
        loading: false,
        error: null
      };

    default:
      return state;
  }
};
