import { createSelector } from "reselect";

// helpers
const getBookById = (books, id) => books.find(book => book.id === id);
const bookCount = (cartItems, bookId) =>
  cartItems.filter(id => bookId === id).length;
const bookTotal = book => book.price * book.count;

// pure selectors
export const booksSelector = state => state.bookList.books;
export const cartItemsSelector = state => state.shoppingCart.cartItems;
// export const orderItem  = state => state.cartItems

// complex selectors
export const getAllShoppingCartItems = createSelector(
  booksSelector,
  cartItemsSelector,
  (books, cartItems) => {
    if (!books.length) return [];
    const uniqueIds = [...new Set(cartItems)];
    const getBooksById = uniqueIds.map(id => getBookById(books, id)); // only unique [{}, {}]

    const updateBooks = getBooksById.map(book => {
      book.count = bookCount(cartItems, book.id);
      book.total = bookTotal(book);
      return book;
    });
    return updateBooks;
  }
);

export const getTotalCartPrice = createSelector(
  booksSelector,
  cartItemsSelector,
  (books, cartItems) => {
    if (!books.length) return 0;
    const getBooksById = cartItems.map(id => getBookById(books, id)); // all [{}, {}]
    const getBooksPrice = getBooksById.map(book => +book.price);
    const totalCartPrice = getBooksPrice.reduce((acc, curr) => acc + curr, 0);
    return totalCartPrice;
  }
);
