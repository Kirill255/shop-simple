import {
  FETCH_BOOKS_REQUEST,
  FETCH_BOOKS_SUCCESS,
  FETCH_BOOKS_FAIL,
  ADD_NEW_BOOK_SUCCESS,
  // ADD_NEW_BOOK_FAIL,
  EDIT_BOOK_SUCCESS,
  DELETE_BOOK_SUCCESS,
  ADD_BOOK_TO_CART,
  REMOVE_BOOK_FROM_CART,
  REMOVE_ALL_BOOK_FROM_CART,
  ADD_NEW_USER,
  PASS_ALL_BOOKS_FROM_BASKET,
  CLEAN_ORDER
} from "../constants";

import userStoreService from "services/userstore-service";
// import FireStoreParser from "firestore-parser";
import { db } from "../firebase";

const booksRequest = () => ({
  type: FETCH_BOOKS_REQUEST
});

const booksLoaded = newBooks => ({
  type: FETCH_BOOKS_SUCCESS,
  payload: newBooks
});

const booksError = error => ({
  type: FETCH_BOOKS_FAIL,
  payload: error
});

export const fetchBooks = () => dispatch => {
  dispatch(booksRequest());
  // bookStoreService
  //   .getBooks()
  //   .then((data) => {
  //     const parsedData = FireStoreParser(data);
  //     console.log(parsedData);
  //     const newData = parsedData.documents.map((doc) => doc.fields);
  //     console.log(newData);
  //     dispatch(booksLoaded(newData));
  //   })
  //   .catch((err) => dispatch(booksError(err)));
  db.collection("books")
    .get()
    .then(data => {
      console.log("data ", data.docs);
      const newData = data.docs.map(doc => ({ id: doc.id, ...doc.data() }));
      console.log("newData ", newData);
      dispatch(booksLoaded(newData));
    })
    .catch(err => dispatch(booksError(err)));
};

export const createBook = book => dispatch => {
  // bookStoreService
  //   .addBook(book)
  db.collection("books")
    .add(book)
    .then(() => {
      dispatch({
        type: ADD_NEW_BOOK_SUCCESS,
        payload: book
      });
    })
    .catch(error => {
      // console.log(error);
      // dispatch({
      //   type: ADD_NEW_BOOK_FAIL,
      //   payload: error
      // });
    });
};

export const editBook = (id, book) => dispatch => {
  // bookStoreService
  db.collection("books")
    .doc(id)
    .update(book)
    .then(() => {
      dispatch({
        type: EDIT_BOOK_SUCCESS,
        payload: { book }
      });
    })
    .catch(error => {
      console.log(error);
    });
};

export const deleteBook = id => dispatch => {
  // bookStoreService
  db.collection("books")
    .doc(id)
    .delete()
    .then(id => {
      dispatch({
        type: DELETE_BOOK_SUCCESS,
        payload: id
      });
    })
    .catch(error => {
      console.log(error);
    });
};

// cart
export const addBookToCart = bookId => ({
  type: ADD_BOOK_TO_CART,
  payload: bookId
});

export const removeBookFromCart = bookId => ({
  type: REMOVE_BOOK_FROM_CART,
  payload: bookId
});

export const removeAllBooksFromCart = bookId => ({
  type: REMOVE_ALL_BOOK_FROM_CART,
  payload: bookId
});

// Add User
export const createNewUser = user => dispatch => {
  console.log(2);
  userStoreService.addUser(user).then(response =>
    dispatch({
      type: ADD_NEW_USER,
      payload: user
    })
  );
};

// Order
export const createOrder = order => dispatch => {
  db.collection("order")
    .add(order)
    .then(() => {
      dispatch({
        type: PASS_ALL_BOOKS_FROM_BASKET,
        payload: order
      });
    })
    .catch(error => {});
};

export const editOrder = (id, order) => dispatch => {
  // bookStoreService
  db.collection("order")
    .doc(id)
    .update(order)
    .then(() => {
      dispatch({
        type: PASS_ALL_BOOKS_FROM_BASKET,
        payload: { order }
      });
    })
    .catch(error => {
      console.log(error);
    });
};

export const cleanOrder = playload => ({
  type: CLEAN_ORDER,
  playload
});
