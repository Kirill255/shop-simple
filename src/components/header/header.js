import React, { useContext } from "react";
import { Link } from "react-router-dom";

import firebaseApp from "../../firebase";
import { AuthContext } from "../../context/auth";

import logo from "./image/logo.png";
import "./header.css";

const Header = () => {
  const { currentUser } = useContext(AuthContext);

  return (
    <header className="header">
      <div className="container">
        <div className="header__logo">
          <Link to="/">
            {" "}
            <div className="logo">
              <img
                className="logo__img"
                src={logo}
                alt="Logo"
                title="My shop"
              />
            </div>
          </Link>
        </div>
        <nav className="header__nav">
          <ul className="nav">
            <li className="nav__item">
              <Link className="nav__link" to="/classic">
                Classic
              </Link>
            </li>
            <li className="nav__item">
              <Link className="nav__link" to="/adventure">
                Adventure
              </Link>
            </li>
            <li className="nav__item">
              <Link className="nav__link" to="/comedy">
                Comedy
              </Link>
            </li>
            <li className="nav__item">
              <Link className="nav__link" to="/fantasy">
                Fantasy
              </Link>
            </li>
            <li className="nav__item">
              <Link className="nav__link" to="/horror">
                Horror
              </Link>
            </li>
          </ul>
        </nav>
      </div>
      <span className="header__auth">
        {!!currentUser ? (
          <Link className="acount" to="/user-account">
            My acount
          </Link>
        ) : (
          <Link className="login" to="/login">
            Login
          </Link>
        )}
      </span>

      {!!currentUser && (
        <div className="sign-out-div">
          <span
            className="signout-btn"
            onClick={() => firebaseApp.auth().signOut()}
          >
            Sign out
          </span>
        </div>
      )}
    </header>
  );
};

export default Header;
