import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import ShopingCardTable from "../shoping-card-table";

// import  {addOrder} from "../../services/order-servise"
import { db } from "../../firebase";
import { /* createOrder, */ cleanOrder } from "../../actions";
import { AuthContext } from "../../context/auth";
import { cartItemsSelector, getTotalCartPrice } from "../../selectors";

import "./rightMenu.css";

class RightMenu extends Component {
  static contextType = AuthContext;

  state = {
    isHovered: false
  };

  handleHover = () => {
    this.setState(prevState => ({
      isHovered: true
    }));
  };

  passData = () => {
    // const userEmail = firebaseApp.auth().currentUser.email;
    // const userid = firebaseApp.auth().currentUser.uid;

    const { currentUser } = this.context;
    const uid = currentUser.uid;
    const email = currentUser.email;

    // console.log(this.props.order);
    // createOrder(this.props.order); // TODO: посмотреть что с этим дублирующим вызовом, нужно оставить что-то одно
    db.collection("order")
      .add({
        user: { uid, email },
        products: this.props.order
      })
      .then(() => {
        console.log("Спасибо за заказ");
        this.props.cleanOrder();
      });
  };

  checkDataOrder = () => {
    // console.log("check, ", this.props.order.length);
    this.props.order.length ? this.passData() : console.log("Корзина пуста");
  };

  getUser = () => {
    const userData = localStorage.getItem("user");
    if (userData) {
      try {
        return JSON.parse(userData);
      } catch (error) {
        console.log(error);
        return null;
      }
    } else {
      return null;
    }
  };

  passOrder = () =>
    this.getUser()
      ? this.checkDataOrder()
      : this.props.history.replace("/login");

  handleHoverOut = () => {
    this.setState(prevState => ({
      isHovered: false
    }));
  };

  render() {
    const { isHovered } = this.state;
    const { totalCartItems, totalCartPrice } = this.props;
    // console.log(order)
    return (
      <div
        className={isHovered ? "right-menu open" : "right-menu clouse"}
        onMouseEnter={this.handleHover}
        onMouseLeave={this.handleHoverOut}
      >
        <div className="shopping-cart">
          <div className="items">
            <div className="item-puck">
              {/* <img className="puck" alt="puck" src={Pucket} /> */}
              <span className="total-item">{totalCartItems}</span>
            </div>

            {!isHovered && (
              <div className="total-prise-clouse"> ${totalCartPrice} </div>
            )}
          </div>
          <div> ${totalCartPrice} </div>
          {/* <AuthContext.Consumer>
        {({usercurrent}) => {
          return <div>{usercurrent.name}</div>
        }}
      </ AuthContext.Consumer> */}
          <span onClick={this.passOrder} className="buy">
            Buy
          </span>
          {/* <AuthContext.Consumer>
          <button onClick={this.passOrder} className="buy">Buy</button>
          </AuthContext.Consumer> */}
        </div>
        <ShopingCardTable isHovered={isHovered} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    totalCartItems: cartItemsSelector(state).length,
    totalCartPrice: getTotalCartPrice(state),
    order: cartItemsSelector(state)
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    { cleanOrder }
  )(RightMenu)
);
