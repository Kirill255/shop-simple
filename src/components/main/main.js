import React from "react";
import "./main.css";
import ShopPage from "./image/shop.png";

const Main = () => {
  return (
    <div>
      <span className="main-content">
        <img src={ShopPage} />
      </span>
    </div>
  );
};

export default Main;
