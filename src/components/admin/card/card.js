import React from "react";

import "./card.css";

const Card = ({ book, handleEditBook, deleteBook }) => {
  const { id, title, author, price, coverImage } = book;

  return (
    <div className="card">
      <div className="card_left">
        <img src={coverImage} alt="" />
      </div>
      <div className="card_right">
        <h2>{title}</h2>
        <div className="card_right__details">
          <ul>
            <li>${price}</li>
            <li>{author}</li>
          </ul>
          <div className="card_right__buttons">
            <button type="button" onClick={() => handleEditBook(book)}>
              edit
            </button>
            <button type="button" onClick={() => deleteBook(id)}>
              delete
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
