import React, { useState, useEffect, useRef } from "react";
import useForm from "react-hook-form";

import "./book-form.css";

const BookForm = ({
  isEditBook = null,
  handleCreateBookSubmit,
  handleEditBookSubmit
}) => {
  const isMounted = useRef(true);
  const [srcCover, setSrcCover] = useState(
    (isEditBook && isEditBook.coverImage) ||
      "http://oksanatsiuryna.com/sites/default/files/placeholder-book-cover-default.png"
  );
  const defaultValues = !isEditBook ? {} : isEditBook;
  const {
    register,
    unregister,
    handleSubmit,
    errors,
    setValue,
    reset /* , formState */
  } = useForm({ mode: "onChange", defaultValues });
  const onSubmit = (data, e) => {
    // console.log("data ", data);
    if (!data.coverImage && !isEditBook) {
      data.coverImage =
        "http://oksanatsiuryna.com/sites/default/files/placeholder-book-cover-default.png";
    }

    if (!isEditBook) {
      handleCreateBookSubmit(data);
    } else {
      handleEditBookSubmit(data);
    }

    if (isMounted.current) {
      e.target.reset();
      reset();
      unregister("coverImage");
      setSrcCover(
        "http://oksanatsiuryna.com/sites/default/files/placeholder-book-cover-default.png"
      );
    }
  };
  // console.log(errors);
  // console.log(JSON.stringify(formState, null, 2));

  function validateCoverImage(file) {
    // console.log("validate ", file);
    if (file && typeof file === "object") {
      if (file.name.lastIndexOf(".") <= 0) {
        return "Invalid file";
      }
      // type: "image/jpeg", "image/png", "image/webp"
      if (file.type.indexOf("image/") < 0) {
        return "Need a picture";
      }
      // limit of 5 MB
      const bytesLimit = 1024 * 1024 * 5; // ~5000000 or 5MB
      if (file.size > bytesLimit) {
        return "Max size 5 MB";
      }
    }
  }

  // register custom field
  useEffect(() => {
    register(
      { name: "coverImage" },
      {
        validate: validateCoverImage
      }
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [register]);

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  const handleChange = e => {
    e.preventDefault();
    const file = e.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.addEventListener("load", e => {
        // set the src url/path to display the picture
        setSrcCover(reader.result);
        // set file as coverImage to submit object data
        setValue("coverImage", file, true);
      });
      reader.readAsDataURL(file);
    }
  };

  return (
    <form className="book-form" onSubmit={handleSubmit(onSubmit)}>
      <h2>{!isEditBook ? "Create book" : "Edit book"}</h2>
      <div>
        <input
          className={errors.title && "error-input"}
          type="text"
          placeholder="Title"
          autoComplete="off"
          name="title"
          ref={register({
            required: "Title required",
            minLength: {
              value: 2,
              message: "Title must be at least 2 characters"
            },
            maxLength: {
              value: 100,
              message: "Title should be no more than 100 characters"
            }
          })}
        />
        {errors.title && <p className="error-text">{errors.title.message}</p>}
      </div>
      <div>
        <input
          className={errors.author && "error-input"}
          type="text"
          placeholder="Author"
          autoComplete="off"
          name="author"
          ref={register({
            required: "Author required",
            minLength: {
              value: 2,
              message: "Author must be at least 2 characters"
            },
            maxLength: {
              value: 100,
              message: "Author should be no more than 100 characters"
            }
          })}
        />
        {errors.author && <p className="error-text">{errors.author.message}</p>}
      </div>
      <div>
        <input
          className={errors.price && "error-input"}
          type="number"
          placeholder="Price"
          autoComplete="off"
          name="price"
          ref={register({
            required: "Price required",
            max: {
              value: 1000000,
              message: "Price can't be greater than 1000000"
            },
            min: { value: 1, message: "Price can't be 0 or less" }
          })}
        />
        {errors.price && <p className="error-text">{errors.price.message}</p>}
      </div>
      <div>
        <input
          onChange={handleChange}
          type="file"
          name="coverImage"
          accept="image/*"
        />
        <div className="book-form__cover-image">
          <img src={srcCover} alt="Cover" height="100" />
        </div>
        {errors.coverImage && (
          <p className="error-text">{errors.coverImage.message}</p>
        )}
      </div>

      <input
        className="btn-submit"
        type="submit"
        value={!isEditBook ? "Create" : "Edit"}
      />
    </form>
  );
};

export default BookForm;

/*
https://codesandbox.io/s/react-hook-form-36444
*/
