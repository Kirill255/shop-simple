import React from "react";

import Add from "./img/add.png";
import "./book-list-item.css";

const BookListItem = ({ book, addBookToCart }) => {
  const { title, author, price, coverImage } = book;
  return (
    <div className="book-list-item">
      <div className="book-cover">
        <img src={coverImage} alt="cover" />
      </div>
      <div className="book-details">
        <div className="author-title">
          <div className="book-title">{title}.</div>
          <div className="book-author">{author}:</div>
        </div>
        <div className="price-and-btn">
          <div className="book-price">${price}</div>
          <span
            onClick={e => {
              e.preventDefault();
              e.nativeEvent.stopPropagation();
              e.nativeEvent.stopImmediatePropagation();
              addBookToCart();
            }}
            className="btn-add-to-cart"
          >
            <img alt="add" className="add" src={Add} />
          </span>
        </div>
      </div>
    </div>
  );
};

export default BookListItem;
