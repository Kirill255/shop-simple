import React, { useContext } from "react";
import { Link } from "react-router-dom";

import { AuthContext } from "../../context/auth";

import "./footer.css";

const Footer = () => {
  const { currentUser } = useContext(AuthContext);

  return (
    <div className="footer">
      <div className="footer-box">
        <span className="here-footer">here will footer</span>
        <ul>
          <li>
            <Link to="/by"> by Semen Yakovenko </Link>
          </li>
          {!!currentUser && (
            <li>
              <Link to="/admin">admin</Link>
            </li>
          )}
        </ul>
      </div>
    </div>
  );
};
export default Footer;
