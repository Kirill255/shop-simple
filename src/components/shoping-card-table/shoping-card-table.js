import React from "react";
import { connect } from "react-redux";

import {
  addBookToCart,
  removeBookFromCart,
  removeAllBooksFromCart
} from "../../actions";
import { getAllShoppingCartItems, getTotalCartPrice } from "../../selectors";

import Delete from "./images/delete.png";
import Minus from "./images/minus.png";
import Plus from "./images/plus.png";
import Check from "./images/check.png";

import "./shoping-card-table.css";

const ShoppingCartTable = ({
  booksInCart,
  totalCartPrice,
  addBookToCart,
  removeBookFromCart,
  removeAllBooksFromCart,
  isHovered
}) => {
  const renderRow = (item, idx) => {
    const { id, title, coverImage, count, total } = item;
    return (
      <div className="cart-data" key={id}>
        <div className="up-part">
          <span>
            {!isHovered && <span className="items-id">{idx + 1}</span>}
            {!isHovered && (
              <div className="cover-image-clouse">
                <img
                  className="cover-imagee"
                  src={coverImage}
                  alt="Cover"
                  title="Cover"
                />
              </div>
            )}
            <img
              className="cover-image"
              src={coverImage}
              alt="Cover"
              title="Cover"
            />
          </span>

          <div className="title">{title}</div>
          <span>
            <span onClick={() => removeAllBooksFromCart(id)}>
              <img className="delet" src={Delete} alt="delete" title="delet" />
            </span>
          </span>
        </div>
        <div className="down-part">
          <div></div>
          <div>
            <span onClick={() => removeBookFromCart(id)}>
              <img className="minus" src={Minus} alt="minus" title="minus" />
            </span>
            <span className="count">{count}</span>
            <span onClick={() => addBookToCart(id)}>
              <img className="plus" src={Plus} alt="plus" title="plus" />
            </span>
          </div>
          <span className="count">${total}</span>
        </div>
      </div>
    );
  };

  return (
    <div className="shopping-cart-table">
      <div className="carts">
        <div className="table">
          <div>{booksInCart.map(renderRow)}</div>
        </div>
      </div>
      <div className="total-block">
        <img className="check" src={Check} alt="check" title="check" />
        <div>Total:</div>
        <div className="total"> ${totalCartPrice}</div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    booksInCart: getAllShoppingCartItems(state),
    totalCartPrice: getTotalCartPrice(state)
  };
};

const mapDispatchToProps = {
  addBookToCart,
  removeBookFromCart,
  removeAllBooksFromCart
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoppingCartTable);
