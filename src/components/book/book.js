import React, { useState, useEffect } from "react";

import Spiner from "../spiner";
import "./book.css";

import { db } from "../../firebase";
import { fetchBooks, addBookToCart } from "../../actions";
import { connect } from "react-redux";

// react-router props приходят из BookPage
const Book = ({ match, addBookToCart }) => {
  const [book, setBook] = useState(null);
  const bookId = match.params.bookId;

  useEffect(() => {
    getBook();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function getBook() {
    try {
      const bookRef = db.collection("books").doc(bookId);
      const doc = await bookRef.get();
      if (!doc.exists) {
        console.log("No such document.");
      } else {
        // console.log(doc.data());
        const bookFromDB = { id: doc.id, ...doc.data() };
        setBook(bookFromDB);
      }
    } catch (error) {
      console.log("Get book error: ", error);
    }
  }
  console.log(book);

  if (!book) {
    return <Spiner />;
  }

  return (
    <div className="book-info-one">
      <div className="general-info">
        <div className="img-book">
          <img src={book.coverImage} alt="" />
        </div>
        <div className="book-info-two">
          <div className="book-title">{book.title}</div>
          <div className="book-author">By {book.author}</div>
          <div className="book-price">${book.price}</div>

          <button onClick={() => addBookToCart(book.id)}>Buy</button>
        </div>
      </div>
      <div className="description">{book.description}</div>

      {/* <pre>{JSON.stringify(book, null, 2)}</pre> */}
    </div>
  );
};
const mapDispatchToProps = dispatch => {
  return {
    fetchBooks: () => dispatch(fetchBooks()),
    addBookToCart: id => dispatch(addBookToCart(id))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Book);
