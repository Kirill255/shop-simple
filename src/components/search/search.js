import React from "react";

const Search = ({ search, handleSearch }) => {
  return (
    <div>
      <input
        type="text"
        placeholder="search..."
        value={search}
        onChange={handleSearch}
      />
    </div>
  );
};

export default Search;
