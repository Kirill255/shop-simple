import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Search from "../search";
import BookListItem from "../book-list-item";
import ErrorIndicator from "../error-indicator";
import Spiner from "../spiner";

import { fetchBooks, addBookToCart } from "../../actions";

import "./book-list.css";

const BookList = ({ books, addBookToCart }) => {
  return (
    <div className="book-box">
      {books.map(book => {
        return (
          <div className="book-info" key={book.id}>
            <Link to={`/book/${book.id}`}>
              <BookListItem
                book={book}
                addBookToCart={() => addBookToCart(book.id)}
              />
            </Link>
          </div>
        );
      })}
    </div>
  );
};

class BookListContainer extends Component {
  state = {
    search: ""
  };

  componentDidMount() {
    this.props.fetchBooks();
  }

  handleSearch = ({ target: { value } }) => {
    this.setState({ search: value });
  };

  render() {
    const { books, loading, error, addBookToCart } = this.props;

    if (error) {
      return <ErrorIndicator />;
    }

    const filteredBooks = books.filter(
      book =>
        book.title.toLowerCase().includes(this.state.search.toLowerCase()) ||
        book.author.toLowerCase().includes(this.state.search.toLowerCase())
    );

    return (
      <div>
        {loading ? (
          <Spiner />
        ) : (
          <div>
            <Search
              search={this.state.search}
              handleSearch={this.handleSearch}
            />
            <BookList books={filteredBooks} addBookToCart={addBookToCart} />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({ bookList: { books, loading, error } }) => ({
  books,
  loading,
  error
});

const mapDispatchToProps = dispatch => {
  return {
    fetchBooks: () => dispatch(fetchBooks()),
    addBookToCart: id => dispatch(addBookToCart(id))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookListContainer);
