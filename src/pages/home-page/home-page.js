import React from "react";

import BookList from "components/book-list";

import "./home-page.css";

const HomePage = () => {
  return (
    <div className="bookslist">
      <BookList />
    </div>
  );
};

export default HomePage;
