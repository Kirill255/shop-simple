// import React, { useState, useEffect } from "react";
// import { connect } from "react-redux";
// import { sha256 } from "js-sha256";

// import { createNewUser } from "../../actions";
// import { validateReg } from "../../utils";

// import "./registration-page.css";

// const initialData = {
//   name: "",
//   email: "",
//   phone: "",
//   password: "",
//   password2: ""
// };

// const Registration = ({ createNewUser }) => {
//   const [user, setUser] = useState(initialData);
//   const [errors, setErrors] = useState({});
//   const [isSubmitting, setSubmitting] = useState(false);

//   useEffect(() => {
//     if (isSubmitting) {
//       const noErrors = Object.keys(errors).length === 0;
//       if (noErrors) {
//         const id = Number(String(Math.random() + 1).split(".")[1]);
//         user.id = id;
//         const newUser = { ...user };
//         delete newUser.password2;
//         newUser.password = sha256(newUser.password);
//         createNewUser(newUser);
//         setUser(initialData);
//         setErrors({});
//         setSubmitting(false);
//       } else {
//         console.log({ errors });
//         setSubmitting(false);
//       }
//     }
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, [isSubmitting]);

//   const handleChange = ({ target: { name, value } }) =>
//     setUser((prevUser) => ({ ...prevUser, [name]: value }));

//   const handleBlur = () => {
//     const validationErrors = validateReg(user);
//     setErrors(validationErrors);
//   };

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     const validationErrors = validateReg(user);
//     setErrors(validationErrors);
//     setSubmitting(true);
//   };

//   return (
//     <form className="registration" onSubmit={handleSubmit}>
//       <div className="reg-logo">
//         <h1>Registration</h1>
//       </div>
//       <div className="input">
//         <div className="form-group-reg">
//           <div className="form-group-reg-name">
//             <input
//               type="text"
//               name="name"
//               value={user.name}
//               onBlur={handleBlur}
//               onChange={handleChange}
//               autoComplete="off"
//               className="dynamic-input-name"
//               placeholder="Enter name"
//             />
//             {errors.name && <p className="error-text">{errors.name}</p>}
//             <label htmlFor="dynamic-input-name">Enter name</label>
//           </div>

//           <div className="form-group-reg-e-mail">
//             <input
//               type="text"
//               name="email"
//               value={user.email}
//               onBlur={handleBlur}
//               onChange={handleChange}
//               autoComplete="off"
//               className="dynamic-input-e-mail"
//               placeholder="Enter e-mail"
//             />
//             {errors.email && <p className="error-text">{errors.email}</p>}
//             <label htmlFor="dynamic-label-input-e-mail">Enter e-mail</label>
//           </div>

//           <div className="form-group-reg-phone">
//             <input
//               type="text"
//               name="phone"
//               value={user.phone}
//               onBlur={handleBlur}
//               onChange={handleChange}
//               autoComplete="off"
//               className="dynamic-input-phone"
//               placeholder="Enter your phone"
//             />
//             {errors.phone && <p className="error-text">{errors.phone}</p>}
//             <label htmlFor="dynamic-label-input-phone">Enter your phone</label>
//           </div>

//           <div className="form-group-reg-password">
//             <input
//               type="password"
//               name="password"
//               value={user.password}
//               onBlur={handleBlur}
//               onChange={handleChange}
//               autoComplete="off"
//               className="dynamic-input-reg-password"
//               placeholder="Enter password"
//             />
//             {errors.password && <p className="error-text">{errors.password}</p>}
//             <label htmlFor="dynamic-label-input-reg-password">Enter password</label>
//           </div>

//           <div className="form-group-reg-password2">
//             <input
//               type="password"
//               name="password2"
//               value={user.password2}
//               onBlur={handleBlur}
//               onChange={handleChange}
//               autoComplete="off"
//               className="dynamic-input-reg-password2"
//               placeholder="Repeat password"
//             />
//             {errors.password2 && <p className="error-text">{errors.password2}</p>}
//             <label htmlFor="dynamic-label-input-reg-password2">Repeat password</label>
//           </div>
//         </div>
//       </div>
//       <div className="btn-registration">
//         <button className="reg-btn" type="submit" disabled={isSubmitting}>
//           Registration
//         </button>
//       </div>
//     </form>
//   );
// };

// export default connect(
//   null,
//   { createNewUser }
// )(Registration);

import React, { useCallback } from "react";
import { withRouter } from "react-router";
import firebaseApp from "../../firebase";

import "./registration-page.css";

const Registration = ({ history }) => {
  const handleRegistration = useCallback(
    async event => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await firebaseApp
          .auth()
          .createUserWithEmailAndPassword(email.value, password.value);
        history.push("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  return (
    <div className="reg-box">
      <div>
        <h1>Registration</h1>
      </div>
      <form onSubmit={handleRegistration}>
        <div className="reg-email">
          <label>
            <input
              className="inpt-reg-email"
              name="email"
              type="email"
              placeholder="Email"
            />
            <hr className="hr-reg" />
          </label>
        </div>
        <div className="reg-password">
          <label>
            <input
              className="inpt-reg-password"
              name="password"
              type="password"
              placeholder="Password"
            />
            <hr className="hr-reg" />
          </label>
        </div>
        <button className="btn-reg" type="submit">
          Registration
        </button>
      </form>
    </div>
  );
};

export default withRouter(Registration);
