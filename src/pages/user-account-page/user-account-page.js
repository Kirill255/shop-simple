import React from "react";

import UserPanel from "containers/user-panel";

import "./user-account-page.css";

const UserAccountPage = () => {
  return (
    <div className="user-account-page">
      <UserPanel />
    </div>
  );
};

export default UserAccountPage;
