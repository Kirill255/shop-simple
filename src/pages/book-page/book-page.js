import React from "react";

import Book from "components/book";

import "./book-page.css";

// прокидываем react-router props, чтобы использовать их внутри Book (id страницы например)
const BookPage = props => {
  return (
    <div className="book-page">
      <Book {...props} />
    </div>
  );
};

export default BookPage;
