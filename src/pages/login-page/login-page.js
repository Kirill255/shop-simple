import React, { useCallback, useContext } from "react";
import { withRouter, Redirect } from "react-router";

import firebaseApp from "../../firebase";
import { AuthContext } from "../../context/auth";

import "./login-page.css";

const Login = ({ history }) => {
  const { currentUser } = useContext(AuthContext);

  const handleLogin = useCallback(
    async event => {
      event.preventDefault();
      const { email, password } = event.target.elements;
      try {
        await firebaseApp
          .auth()
          .signInWithEmailAndPassword(email.value, password.value);
        history.replace("/");
      } catch (error) {
        alert(error);
      }
    },
    [history]
  );

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <div className="login-box">
      <div>
        <h1>Log in</h1>
      </div>
      <form className="login-form" onSubmit={handleLogin}>
        <div className="log-email">
          <label>
            <input
              className="inpt-log-email"
              name="email"
              type="email"
              placeholder="Email"
            />
            <hr className="hr-log" />
          </label>
        </div>
        <div className="log-password">
          <label>
            <input
              className="inpt-log-password"
              name="password"
              type="password"
              placeholder="Password"
            />
            <hr className="hr-log" />
          </label>
        </div>
        <button className="btn-login" type="submit">
          Log in
        </button>
      </form>
    </div>
  );
};

export default withRouter(Login);
