import React from "react";

import AdminPanel from "containers/admin-panel";

const AdminHomePage = () => {
  return (
    <div className="admin-page">
      <AdminPanel />
    </div>
  );
};

export default AdminHomePage;
