import React from "react";
import "./join.css";
import { Link } from "react-router-dom";

const Join = () => {
  return (
    <div className="join-custumer">
      <div className="join-data">
        <div className="join-logo">
          <h1 className="join-logo">Join to as!</h1>
        </div>
        <div className="join-texts">
          <p className="join-text">
            {" "}
            Get bonuses for <br /> your orders and <br /> keep order history
          </p>
        </div>
      </div>
      <div className="join-register-btn">
        <Link className="registration" to="/registration">
          registration
        </Link>
      </div>
    </div>
  );
};
export default Join;
