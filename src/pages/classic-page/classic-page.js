import React, { Component } from "react";

import "./classic-page.css";

class ClassicPage extends Component {
  render() {
    return (
      <div>
        <h1>Classic Page</h1>
      </div>
    );
  }
}

export default ClassicPage;
