import HomePage from "./home-page";
import BookPage from "./book-page";
import CartPage from "./cart-page";
import AdminPage from "./admin-page";
import ClassicPage from "./classic-page";
import AdventurePage from "./adventure-page";
import ComedyPage from "./comedy-page";
import FantasyPage from "./fantasy-page";
import HorrorPage from "./horror-page";
import RegistrationPage from "./registration-page";
import LoginPage from "./login-page";
import UserAccountPage from "./user-account-page";

export {
  HomePage,
  BookPage,
  CartPage,
  AdminPage,
  ClassicPage,
  AdventurePage,
  ComedyPage,
  FantasyPage,
  HorrorPage,
  RegistrationPage,
  LoginPage,
  UserAccountPage
};
