import React from "react";

import AdminPanel from "containers/admin-panel";

const AdminPage = () => {
  return (
    <div className="admin-page">
      <AdminPanel />
    </div>
  );
};

export default AdminPage;
