import React, { useState, useEffect, useContext } from "react";

import { AuthContext } from "../../context/auth";

import "./user-panel.css";

const UserPanel = () => {
  const { currentUser } = useContext(AuthContext);
  const [email, setEmail] = useState("");

  useEffect(() => {
    setEmail(currentUser.email);
  }, [currentUser]);

  return (
    <div>
      <p>User panel {email}</p>
    </div>
  );
};

export default UserPanel;
