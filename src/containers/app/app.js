import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import {
  HomePage,
  BookPage,
  CartPage,
  AdminPage,
  ClassicPage,
  AdventurePage,
  ComedyPage,
  FantasyPage,
  HorrorPage,
  RegistrationPage,
  LoginPage,
  UserAccountPage
} from "pages";

import { AuthProvider } from "../../context/auth";
import PrivateRoute from "../private-route";
import AdminPrivateRoute from "../admin-private-route";

import {
  withDefaultLayout,
  withAdminLayout,
  withAuthLayout,
  withUserAccountLayout
} from "hoc";

import "./app.css";

const App = () => (
  <div>
    <AuthProvider>
      <Router>
        <Switch>
          {/* <PrivateRoute path="/" component={HomePage} exact /> */}
          {/* <PrivateRoute path="/" render={withDefaultLayout(HomePage)} exact /> */}

          <Route path="/" render={withDefaultLayout(HomePage)} exact />
          <Route path="/book/:bookId" render={withDefaultLayout(BookPage)} />
          <Route path="/cart" render={withDefaultLayout(CartPage)} />
          <Route path="/classic" render={withDefaultLayout(ClassicPage)} />
          <Route path="/adventure" render={withDefaultLayout(AdventurePage)} />
          <Route path="/comedy" render={withDefaultLayout(ComedyPage)} />
          <Route path="/fantasy" render={withDefaultLayout(FantasyPage)} />
          <Route path="/horror" render={withDefaultLayout(HorrorPage)} />
          {/* <Route path="/acount" render={withDefaultLayout(MyAcount)} /> */}
          <Route
            path="/registration"
            component={withAuthLayout(RegistrationPage)}
          />
          <Route path="/login" component={withAuthLayout(LoginPage)} />
          <PrivateRoute
            path="/user-account"
            render={withUserAccountLayout(UserAccountPage)}
          />
          <AdminPrivateRoute
            path="/admin"
            render={withAdminLayout(AdminPage)}
          />
          <Route render={() => <h1>Page not found</h1>} />
        </Switch>
      </Router>
    </AuthProvider>
  </div>
);

export default App;
