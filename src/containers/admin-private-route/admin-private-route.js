import React, { useState, useEffect, useContext, useRef } from "react";
import { Route, Redirect } from "react-router-dom";

import Spiner from "components/spiner";

import { AuthContext } from "../../context/auth";
import { db } from "../../firebase";

const AdminPrivateRoute = ({ render: RouteComponent, ...rest }) => {
  const { currentUser } = useContext(AuthContext);
  const [isAdmin, setIsAdmin] = useState(null);
  const [loading, setLoading] = useState(true);
  const isMounted = useRef(true);

  useEffect(() => {
    db.collection("users")
      .get()
      .then(data => {
        if (!currentUser) {
          setLoading(false);
          return;
        }
        const users = data.docs.map(doc => ({ id: doc.id, ...doc.data() }));
        // console.log("currentUser ", currentUser.uid); // вот это просто заккоментируй
        // может быть несколько админов
        const check = users.some(user => currentUser.uid === user.id);

        if (check) {
          if (isMounted.current) {
            setIsAdmin(true);
            setLoading(false);
          }
        } else {
          if (isMounted.current) {
            setIsAdmin(null);
            setLoading(false);
          }
        }
      })
      .catch(err => {
        console.log(err);
      });

    return () => {
      isMounted.current = false;
    };
  }, [currentUser]);

  return (
    <>
      {loading ? (
        <Spiner />
      ) : (
        <Route
          {...rest}
          render={routeProps =>
            !!currentUser && isAdmin ? (
              <RouteComponent {...routeProps} />
            ) : !!currentUser ? (
              <Redirect to={"/user-account"} />
            ) : (
              <Redirect to={"/login"} />
            )
          }
        />
      )}
    </>
  );
};

export default AdminPrivateRoute;
