import React, { useState } from "react";
import { connect } from "react-redux";

import BookForm from "components/admin/book-form";
import Card from "components/admin/card";

import { createBook, editBook, deleteBook } from "../../actions";
import ImageUploaderService from "../../services/image-uploader-service";

import "./admin-panel.css";

const AdminPanel = ({ createBook, editBook, deleteBook, books }) => {
  const [isEditBook, setIsEditBook] = useState(null);

  const handleEditBook = book => {
    setIsEditBook(book);
  };

  const handleCreateBookSubmit = async book => {
    // console.log("book ", book);
    // const id = Number(String(Math.random() + 1).split(".")[1]);
    // book.id = id;
    if (typeof book.coverImage === "object") {
      const { data } = await ImageUploaderService.uploadImage(book.coverImage);
      // console.log(data);
      book.coverImage = data.url;
    }
    createBook(book);
  };

  // trick with id, because react-hook-form, loses all unused fields of an object
  const handleEditBookSubmit = async book => {
    // console.log("book ", book);
    book.id = isEditBook.id;
    if (typeof book.coverImage === "object") {
      const { data } = await ImageUploaderService.uploadImage(book.coverImage);
      book.coverImage = data.url;
    }
    editBook(isEditBook.id, book);
    setIsEditBook(null);
  };

  return (
    <div className="admin-panel">
      <div className="books">
        <h2>Books</h2>
        <ul>
          {books.map((book, i) => (
            <li key={i}>
              <Card
                book={book}
                handleEditBook={handleEditBook}
                deleteBook={deleteBook}
              />
            </li>
          ))}
        </ul>
      </div>
      <div>
        <BookForm handleCreateBookSubmit={handleCreateBookSubmit} />

        {isEditBook && (
          <BookForm
            isEditBook={isEditBook}
            handleEditBookSubmit={handleEditBookSubmit}
          />
        )}
      </div>
    </div>
  );
};

export default connect(
  ({ bookList: { books } }) => ({ books }),
  { createBook, editBook, deleteBook }
)(AdminPanel);
