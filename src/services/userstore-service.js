const baseURL = "http://localhost:3004/users";

class UserstoreService {
  addUser(user) {
    console.log(3);
    return fetch(baseURL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    }).then(res => res.json());
  }
}

export default new UserstoreService();
