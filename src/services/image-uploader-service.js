// import axios from "axios";
const CLIENT_API_KEY = "3f5504d27c8a90abf2bb9177d9a75293";
const baseURL = `https://api.imgbb.com/1/upload?key=${CLIENT_API_KEY}`;

class ImageUploaderService {
  uploadImage(image) {
    const formData = new FormData();
    formData.append("image", image);

    return fetch(baseURL, {
      method: "POST",
      body: formData
    }).then(res => res.json());
  }
}

// class ImageUploaderService {
//   uploadImage(image) {
//     console.log(image);
//     var formData = new FormData();
//     formData.append("image", image);
//     axios
//       .post(baseURL, formData)
//       .then(function(response) {
//         console.log(response);
//       })
//       .catch(function(error) {
//         console.log(error.response);
//       });
//   }
// }

export default new ImageUploaderService();

/*
https://stackoverflow.com/questions/46640024/how-do-i-post-form-data-with-fetch-api
*/

/*
https://mrshop.imgbb.com/
qbh32965@bcaoo.com
mrshop
m7h*w52fuw
*/
