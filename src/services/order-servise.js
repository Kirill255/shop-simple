const baseURL =
  "https://firestore.googleapis.com/v1/projects/books-3f8e0/databases/(default)/documents/order";

class OrderService {
  addOrder(order) {
    return fetch(baseURL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(order)
    }).then(res => res.json());
  }
}
export default new OrderService();
