const baseURL =
  "https://firestore.googleapis.com/v1/projects/books-3f8e0/databases/(default)/documents/books";

class BookstoreService {
  getBooks() {
    return fetch(baseURL).then(res => res.json());
  }

  getBook(id) {
    return fetch(`${baseURL}/${id}`).then(res => res.json());
  }

  addBook(book) {
    return fetch(baseURL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(book)
    }).then(res => res.json());
  }

  editBook(id, book) {
    return fetch(`${baseURL}/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(book)
    }).then(res => res.json());
  }

  deleteBook(id) {
    return fetch(`${baseURL}/${id}`, {
      method: "DELETE"
      // headers: {
      //   "Content-Type": "application/json"
      // }
    });
  }
}

export default new BookstoreService();
