import React from "react";

import Header from "../../components/header";
import Footer from "../../components/footer";
import RightMenu from "../../components/rightMenu";

const defaultLayout = Component => props => (
  <div>
    <Header />
    <div className="main-container">
      <RightMenu />
      <Component {...props} />
    </div>
    <Footer />
  </div>
);

export default defaultLayout;
