import React from "react";
import { Link } from "react-router-dom";

const withAuthLayout = Component => props => {
  const currentPageName = props.match.path.slice(1); // /login -> login
  const forwardToPage = currentPageName === "login" ? "registration" : "login";
  // console.log(props);
  return (
    <div style={{ display: "flex", flexDirection: "column", height: "100vh" }}>
      <header
        style={{
          display: "flex",
          alignItems: "center",
          flex: "0 0 auto",
          background: "green",
          minHeight: "50px"
        }}
      >
        <div style={{ padding: "20px", width: "100%" }}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <h1 style={{ margin: 0, padding: 0, textTransform: "capitalize" }}>
              {currentPageName}
            </h1>
            <div>
              <Link to="/" style={{ marginRight: "5px" }}>
                <button
                  style={{ padding: "2px 3px", cursor: "pointer" }}
                  type="button"
                >
                  back home
                </button>
              </Link>
              <Link to={`/${forwardToPage}`}>
                <button
                  style={{
                    padding: "2px 3px",
                    cursor: "pointer",
                    textTransform: "capitalize"
                  }}
                  type="button"
                >
                  {forwardToPage}
                </button>
              </Link>
            </div>
          </div>
        </div>
      </header>
      <div style={{ flex: "1 0 auto" }}>
        <div style={{ padding: "20px" }}>
          <Component {...props} />
        </div>
      </div>
      <footer
        style={{ flex: "0 0 auto", background: "green", minHeight: "50px" }}
      >
        <div style={{ padding: "20px" }}>
          <p style={{ margin: 0, padding: 0 }}>&copy; 2019</p>
        </div>
      </footer>
    </div>
  );
};

export default withAuthLayout;
