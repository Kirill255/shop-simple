import React from "react";
import { Link } from "react-router-dom";

const withUserAccountLayout = Component => props => (
  <div style={{ display: "flex", flexDirection: "column", height: "100vh" }}>
    <header
      style={{
        display: "flex",
        alignItems: "center",
        flex: "0 0 auto",
        background: "Aquamarine",
        minHeight: "50px"
      }}
    >
      <div style={{ padding: "20px", width: "100%" }}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <h1 style={{ margin: 0, padding: 0 }}>User Account</h1>
          <div>
            <Link to="/">
              <button
                style={{ padding: "2px 3px", cursor: "pointer" }}
                type="button"
              >
                back home
              </button>
            </Link>
          </div>
        </div>
      </div>
    </header>
    <div style={{ flex: "1 0 auto" }}>
      <div style={{ padding: "20px" }}>
        <Component {...props} />
      </div>
    </div>
    <footer
      style={{ flex: "0 0 auto", background: "Aquamarine", minHeight: "50px" }}
    >
      <div style={{ padding: "20px" }}>
        <p style={{ margin: 0, padding: 0 }}>&copy; 2019</p>
      </div>
    </footer>
  </div>
);

export default withUserAccountLayout;
